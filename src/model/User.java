package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ben on 4/6/2018.
 */
public class User implements Comparable<User>,Serializable {

    public static final long serialVersionID = 1L;
    private String username;
    private String password;
    public ArrayList<Album> userAlbums;

    public User(String name, String pass){
        username = name;
        password = pass;
        userAlbums = new ArrayList<Album>();
    }

    @Override
    public int compareTo(User a){
        return username.compareTo(a.username);
    }

    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

    public ArrayList<Album> getUserAlbums() {
        return userAlbums;
    }
}
