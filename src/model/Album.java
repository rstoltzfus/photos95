package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Ben on 4/6/2018.
 */
public class Album implements Comparable<Album>,Serializable{

    private static final long SerialVersionID = 1L;
    private String albumName;
    private ArrayList<Photo> album;

    public Album(String name){
        albumName = name;
        album = new ArrayList<Photo>();
    }

    public Album(String name, ArrayList<Photo> photos){
        albumName = name;
        album = photos;
    }

    public String getAlbumName(){ return albumName; }
    public void setAlbumName(String name){ albumName = name; }
    public ArrayList<Photo> getAlbum() { return album; }
    public void setAlbum(ArrayList<Photo> newAlbum){ album = newAlbum; }
    public void addPhoto(Photo photo){ album.add(photo); }
    public void removePhoto(Photo photo){ album.remove(photo); }
    public int getAlbumSize(){ return album.isEmpty() ? 0 : album.size(); }

    public String getfirstDate(){
        if (album.isEmpty())
            return "No photos found";
        String temp = album.get(0).getDate();
        for(int i = 1; i < album.size(); i++){
            if (temp.compareTo(album.get(i).getDate()) < 0)
                temp = album.get(i).getDate();
        }
        return temp;
    }

    public String getLastDate(){
        if (album.isEmpty())
            return "No photos found";
        String temp = album.get(0).getDate();
        for(int i = 1; i < album.size(); i++){
            if (temp.compareTo(album.get(i).getDate()) > 0)
                temp = album.get(i).getDate();
        }
        return temp;
    }

    @Override
    public int compareTo(Album a){ return albumName.compareTo(a.albumName); }


}
