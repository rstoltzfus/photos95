package model;

import javafx.scene.image.Image;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Ben on 4/6/2018.
 */
public class Photo implements Serializable {

    private static final long serialVersionID = 1L;
    private String photoName;
    private String caption;
    private File file;
    private Calendar calender;
    private ArrayList<Tag> tags;

    public Photo(File f, String name, String cap){

        file = f;
        caption = cap;
        photoName = name;
        tags = new ArrayList<Tag>();
        calender = new GregorianCalendar();
        calender.setTime(new Date(f.lastModified()));
        calender.set(calender.MILLISECOND, 0);

    }

    public Boolean addTag(Tag tag){
        for (Tag t : tags) {
            if (t.getTagName().equalsIgnoreCase(tag.getTagName())
                    && t.getTagValue().equalsIgnoreCase(tag.getTagValue())) {
                return false;
            }
        }
        tags.add(tag);
        return true;
    }
    public void deleteTag(Tag tag){
        for(int i = 0; i < tags.size(); i++){
            if (tags.get(i).getTagName().equalsIgnoreCase(tag.getTagName()) && tags.get(i).getTagValue().equalsIgnoreCase(tag.getTagValue())) {
                tags.remove(i);
                break;
            }
        }
    }
    public ArrayList<Tag> getTags(){ return tags; }
    public String getTagString() {
        String tagString = "";
        for (Tag t : tags) { tagString = tagString.concat(t.toString()); }
        return tagString;
    }
    public String getPhotoName(){ return photoName; }
    public void setPhotoName(String name){ photoName = name; }
    public String getDate(){
        DateFormat date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        return date.format(this.calender.getTime());
    }
    public LocalDate getLocalDate() {
        return this.calender.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    public void setCaption(String newCaption){ caption = newCaption; }
    public String getCaption(){ return caption; }
    public File getFile(){ return file; }
    public Image getThumbnail() { return new Image(file.toURI().toString()); }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Photo)) return false;
        Photo other = (Photo)o;
        return this.photoName.equals(other.photoName) && this.caption.equals(other.caption)
                && this.file.equals(other.file) && this.tags.equals(other.tags);
    }
}
