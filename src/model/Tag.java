package model;

import java.io.Serializable;

/**
 * Created by Ben on 4/6/2018.
 */
public class Tag implements Serializable{

    public static final long SerialVersionID = 1L;
    private String tagName;
    private String tagValue;

    public Tag(String name, String value){
        tagName = name;
        tagValue = value;
    }

    public String getTagName() {
        return tagName;
    }
    public String getTagValue() {
        return tagValue;
    }
    public boolean equals(Tag tag) {
        return tag.getTagName().equalsIgnoreCase(tagName) && tag.getTagValue().equalsIgnoreCase(tagValue);
    }
    public String toString(){
        return "(" + tagName + ": " + tagValue + ")";
    }
}

