package app;

/**
 *Class to create and run a functional photos library.
 *@author Benjamin Cahnbley
 *@author Richard Stoltzfus
 */
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
//import view.*;

public class Photos extends Application{

    /**
     *Starts the main stage for photos.
     * @param primaryStage Starting stage
     * @throws Exception exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception{

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation((getClass().getResource("/view/Login.fxml")));
        GridPane root = (GridPane)loader.load();

        Scene scene = new Scene(root, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Photo Library");
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }
}
