package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.ArrayList;

public class SlideshowController {
    @FXML private MenuBar mainMenu;
    @FXML private Menu fileMenu;
    @FXML private MenuItem exitItem;
    @FXML private Button nextButton;
    @FXML private Button prevButton;
    @FXML private ImageView currentImage;
    @FXML private Label photoTags;

    private ArrayList<Image> currentAlbum;
    private ArrayList<String> tagList;
    private int currentPhoto;
    private String albumName;

    void start(Stage mainStage) {
        currentImage.setImage(currentAlbum.get(currentPhoto));
        photoTags.setText("Tags: " + tagList.get(currentPhoto));
        if (!hasPrevious()) prevButton.setDisable(true); else prevButton.setDisable(false);
        if (!hasNext()) nextButton.setDisable(true); else nextButton.setDisable(false);
    }

    void setCurrentAlbum(ArrayList<Image> album) { currentAlbum = album; }
    void setPhotoTags(ArrayList<String> tags) {tagList = tags; }
    void setCurrentPhoto(int index) { currentPhoto = index; }
    void setAlbumName(String name) { albumName = name; }

    private Boolean hasPrevious() { return currentPhoto > 0; }
    private Boolean hasNext() { return currentPhoto < (currentAlbum.size() - 1); }

    @FXML
    private void prevPhoto(){
        if (hasPrevious()) {
            currentPhoto--;
            currentImage.setImage(currentAlbum.get(currentPhoto));
            photoTags.setText("Tags: " + tagList.get(currentPhoto));
        }
        if (!hasPrevious()) prevButton.setDisable(true); else prevButton.setDisable(false);
        if (!hasNext()) nextButton.setDisable(true); else nextButton.setDisable(false);
    }

    @FXML
    private void nextPhoto(){
        if (hasNext()) {
            currentPhoto++;
            currentImage.setImage(currentAlbum.get(currentPhoto));
            photoTags.setText("Tags: " + tagList.get(currentPhoto));
        }
        if (!hasPrevious()) prevButton.setDisable(true); else prevButton.setDisable(false);
        if (!hasNext()) nextButton.setDisable(true); else nextButton.setDisable(false);
    }

    @FXML
    private void exit() {
        try {
            mainMenu.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Album.fxml"));
            Parent root = loader.load();
            AlbumController ctrl = loader.getController();
            ctrl.setCurrentAlbumName(albumName);

            Stage stage = new Stage();
            ctrl.start(stage);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Photo Explorer");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

}
