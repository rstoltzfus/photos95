package controller;

import model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.util.logging.LogManager;


/**
 * Class that allows admin to create/delete users
 * @author Benjamin Cahnbley
 * @author Richard Stoltzfus
 */
public class AdminController {

    @FXML private Button add;
    @FXML private Button delete;

    @FXML private TextField addUsername;
    @FXML private TextField addPassword;
    @FXML private Label usernameLabel;
    @FXML private Label addPasswordLabel;

    @FXML private ButtonBar addBar;
    @FXML private ButtonBar deleteBar;
    @FXML private ButtonBar topBar;

    @FXML
    private ListView<String> users;
    private ObservableList<String> observableUserList;


    public void start(Stage mainStage){

        if(observableUserList == null){
            if (LoginController.userList.size() == 0){
                observableUserList = FXCollections.observableArrayList();
                users.setItems(observableUserList);
            }
            else{
                observableUserList = FXCollections.observableArrayList(LoginController.getUsersList(LoginController.userList));
                users.setItems(observableUserList);
            }
        }
        usernameLabel.setVisible(false);
        addPasswordLabel.setVisible(false);
        addUsername.setDisable(true);
        addUsername.setVisible(false);
        addPassword.setDisable(true);
        addPassword.setVisible(false);

        addBar.setDisable(true);
        addBar.setVisible(false);
        deleteBar.setDisable(true);
        deleteBar.setVisible(false);

    }

    /**
     * method that sets up ability to add new user.
     * displays text boxes to input required info to generate new user 
     * @param e button click event
     */
    public void add(ActionEvent e){

        topBar.setDisable(true);
        addBar.setDisable(false);
        addBar.setVisible(true);
        usernameLabel.setVisible(true);
        addPasswordLabel.setVisible(true);
        addUsername.setDisable(false);
        addUsername.setVisible(true);
        addPassword.setDisable(false);
        addPassword.setVisible(true);

    }

    /**
     * Confirm button for deleting a user
     * @param e button click event
     */
    public void confirmDelete(ActionEvent e){

        topBar.setDisable(true);

        if (users.getSelectionModel().getSelectedItem() == null)
            return;
        if(observableUserList.size() == 0)
            return;

        int index = users.getSelectionModel().getSelectedIndex();

        deleteDirectory(new File("Users/" + users.getSelectionModel().getSelectedItem()));

        LogManager.getLogManager().reset();
        for(int i = 0; i < LoginController.userList.size(); i++){
            if(users.getSelectionModel().getSelectedItem().equals(LoginController.userList.get(i).getUsername()))
                LoginController.userList.remove(LoginController.userList.get(i));
        }
        observableUserList.remove(index,index+1);
        users.setItems(observableUserList);
        users.getSelectionModel().select(index);

        topBar.setDisable(false);
        deleteBar.setDisable(true);
        deleteBar.setVisible(false);

        LoginController.write(LoginController.userList);

    }
    
    /**
     * Function that deletes the folder designated by user.
     * Recursively deletes all files within before deleting empty folder
     * @param f file object that is to be deleted
     * @return boolean for successful deletion
     */
    public static boolean deleteDirectory(File f){
        if(f.isDirectory()){
            File[] files = f.listFiles();
            for(int i =0; i < files.length; i++) {
                boolean success = deleteDirectory(files[i]);
                if (!success)
                    return false;
            }

        }
        return f.delete();
    }

    /**
     * Function that confirms the addition of adding new user
     * @param e button click event
     */
    public void confirmAdd(ActionEvent e){
        LogManager.getLogManager().reset();
        for(int i = 0; i < LoginController.userList.size(); i++){
            if(LoginController.userList.get(i).getUsername().equalsIgnoreCase(addUsername.getText().trim())){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error: Duplicate username.");
                alert.setResizable(false);
                alert.showAndWait();
                return;
            }
        }

        if(addUsername.getText().trim().equals("") || addUsername.getText().trim().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: Missing username or password.");
            alert.setResizable(false);
            alert.showAndWait();
            return;
        }

        User newUser = new User(addUsername.getText(),addPassword.getText());

        LoginController.userList.add(newUser);
        observableUserList.add(addUsername.getText().trim());
        users.setItems(observableUserList);
        for(int i = 0; i < observableUserList.size(); i++){
            if(observableUserList.get(i).equals(addUsername.getText().trim())){
                users.getSelectionModel().select(i);
                break;
            }
        }
        LoginController.write(LoginController.userList);

        topBar.setDisable(false);
        usernameLabel.setVisible(false);
        addPasswordLabel.setVisible(false);
        addUsername.setDisable(true);
        addUsername.setVisible(false);
        addPassword.setDisable(true);
        addPassword.setVisible(false);
        addBar.setDisable(true);
        addBar.setVisible(false);

        new File("Users").mkdir();
        new File("Users/" + addUsername.getText()).mkdir();

    }

    /**
     * Function that sets up ability to delete an existing user.
     * asks for confirmation that currently selected user should be deleted.
     * @param e button click event
     */
    public void delete(ActionEvent e){
        topBar.setDisable(true);
        deleteBar.setVisible(true);
        deleteBar.setDisable(false);
    }

    /**
     * Cancels current action attempt and returns to original settings.
     * @param e button click event
     */
    public void cancel(ActionEvent e){
        addBar.setDisable(true);
        addBar.setVisible(false);
        deleteBar.setDisable(true);
        deleteBar.setVisible(false);

        topBar.setDisable(false);

        usernameLabel.setVisible(false);
        addPasswordLabel.setVisible(false);
        addUsername.setDisable(true);
        addUsername.setVisible(false);
        addPassword.setDisable(true);
        addPassword.setVisible(false);
        usernameLabel.setVisible(false);
    }

    /**
     * function to log out of being admin and returns to Login screen.
     * @param e button click event
     */
    public void logout(ActionEvent e){
        try {

            ((Node) e.getSource()).getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Login.fxml"));
            Parent root = loader.load();

            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("login");
            stage.setScene(new Scene(root, 300, 250));
            stage.setResizable(false);

            stage.show();

        }
        catch (Exception e2){
            e2.printStackTrace();
        }
    }

}
