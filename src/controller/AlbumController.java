package controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import model.Album;
import model.Photo;
import model.Tag;
import model.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Optional;

/**
 * Class that shows images and captions for a specific album.
 * Other functions to change/add/delete photos also exists
 * @author Benjamin Cahnbley
 * @author Richard Stoltzfus
 */
public class AlbumController {

    @FXML private MenuBar mainMenu;
    @FXML private Menu fileMenu;
    @FXML private Menu fileMenuNew;
    @FXML private MenuItem newPhotoItem;
    @FXML private MenuItem logoutItem;
    @FXML private MenuItem exitItem;
    @FXML private Menu editMenu;
    @FXML private MenuItem renameItem;
    @FXML private MenuItem deleteItem;
    @FXML private Menu photoMenu;
    @FXML private MenuItem captionItem;
    @FXML private Menu photoTagMenu;
    @FXML private MenuItem addTagItem;
    @FXML private MenuItem removeTagItem;
    @FXML private MenuItem copyToItem;
    @FXML private MenuItem moveToItem;
    @FXML private MenuItem slideshowItem;

    @FXML private Label albumLabel;
    @FXML private Label captionLabel;
    @FXML private ListView<ImageView> photoView;

    @FXML private TextField actionField;
    @FXML private TextField tagField;

    @FXML private ButtonBar confirmBar;
    @FXML private Button confirmButton;
    @FXML private Button cancelButton;

    @FXML private Menu searchMenu;
    @FXML private MenuItem photoSearchItem;
    @FXML private MenuItem searchCreateItem;

    private static ArrayList<Photo> album;
    private static User currentUser;
    private String currentAlbumName;
    private Album currentAlbum;
    private ArrayList<Photo> searchAlbum;
    private ArrayList<Album> albumList;
    private ObservableList<ImageView> observableImageList;
    private int action;
    private static final String FILENAME = "albumList.txt";

    /**
     * Starts current stage where being within a specified album
     * @param mainStage stage for an album that is selected
     */
    void start(Stage mainStage) {

        albumList = UserController.getAlbumList();
        currentAlbum = new Album(currentAlbumName);
        currentAlbum.setAlbum(album);
        ArrayList<ImageView> result = new ArrayList<>();
        for(Photo p: album) {
            ImageView tmp =new ImageView(p.getThumbnail());
            tmp.setFitWidth(100);
            tmp.setFitHeight(100);
            result.add(tmp);
        }
        observableImageList = FXCollections.observableArrayList(result);
        photoView.setItems(observableImageList);
        photoView.getSelectionModel().select(0);
        if (!album.isEmpty()) { captionLabel.setText("Caption: " + album.get(0).getCaption()); }

        actionField.setVisible(false);
        actionField.setDisable(true);
        tagField.setVisible(false);
        tagField.setDisable(true);
        confirmBar.setVisible(false);
        searchMenu.setVisible(false);
        photoSearchItem.setVisible(false);
        searchCreateItem.setVisible(false);

        if (currentAlbumName != null) {
            albumLabel.setText(currentAlbumName + ": ");
        }
        else {
            albumLabel.setText("Search Results: ");
        }
    }
    
    /**
     * Sets the album used from another class
     * @param newAlbum photo ArrayList of album to be set as global
     */
    public static void setAlbum(ArrayList<Photo> newAlbum){ album = newAlbum; }
    
    /**
     * General confirmation function to confirm actions
     */
    public void confirm() {
        switch(action){
            //rename
            case(1):{
                int index = photoView.getSelectionModel().getSelectedIndex();
                album.get(index).setPhotoName(actionField.getText());
                photoView.getSelectionModel().select(index);
                photoView.scrollTo(index);
                captionLabel.setText("Caption: " + album.get(index).getCaption());
                break;
            }
            //delete
            case(2):{
                int index = photoView.getSelectionModel().getSelectedIndex();
                album.remove(index);
                observableImageList.remove(index);
                photoView.setItems(observableImageList);
                photoView.getSelectionModel().select(0);
                photoView.scrollTo(0);
                if(album.size() != 0)
                    captionLabel.setText("Caption: " + album.get(0).getCaption());
                else {
                    captionLabel.setText("Caption: Album empty");
                    captionLabel.setVisible(false);
                    captionLabel.setDisable(true);
                }
                break;
            }
            //change Caption
            case(3):{
                int index = photoView.getSelectionModel().getSelectedIndex();
                album.get(index).setCaption(actionField.getText());
                photoView.getSelectionModel().select(index);
                photoView.scrollTo(index);
                captionLabel.setText("Caption: " + actionField.getText());
                break;
            }
            // add tag
            case(4):{
                int index = photoView.getSelectionModel().getSelectedIndex();
                Tag newTag = new Tag(actionField.getText(),tagField.getText());
                if (!album.get(index).addTag(newTag)) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Alert");
                    alert.setHeaderText("Tag already exists.");
                    alert.setResizable(false);
                    alert.showAndWait();
                }
                photoView.getSelectionModel().select(index);
                photoView.scrollTo(index);
                captionLabel.setText("Caption: " + album.get(index).getCaption());
                actionField.setText("");
                break;
            }
            //remove tag
            case(5):{
                int index = photoView.getSelectionModel().getSelectedIndex();
                Tag newTag = new Tag(actionField.getText(),tagField.getText());
                album.get(index).deleteTag(newTag);
                photoView.getSelectionModel().select(index);
                photoView.scrollTo(index);
                captionLabel.setText("Caption: " + album.get(index).getCaption());
                break;
            }
            //copyTo
            case(6):{
                boolean found = false;
                albumList = read();
                for (Album a: albumList){
                    if(a.getAlbumName().equalsIgnoreCase(actionField.getText())){
                        found = true;
                        int index = photoView.getSelectionModel().getSelectedIndex();
                        a.addPhoto(album.get(index));
                        photoView.getSelectionModel().select(index);
                        photoView.scrollTo(index);
                        captionLabel.setText("Caption: " + album.get(index).getCaption());
                        //"Photo copied successfully"?
                        break;
                    }
                }
                if(!found){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error: Album not found.");
                    alert.setResizable(false);
                    alert.showAndWait();
                    break;
                }
                write(albumList);
                break;
            }
            //move
            case(7):{
                boolean found = false;
                albumList = read();
                for (Album a: albumList){
                    if(a.getAlbumName().equalsIgnoreCase(actionField.getText())){
                        int index = photoView.getSelectionModel().getSelectedIndex();
                        a.addPhoto(album.get(index));
                        found = true;
                        break;
                    }

                }
                if(!found){
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Error: Album not found.");
                    alert.setResizable(false);
                    alert.showAndWait();
                    break;
                }

                for (Album a: albumList){
                    if(a.getAlbumName().equalsIgnoreCase(currentAlbumName)){
                        int index = photoView.getSelectionModel().getSelectedIndex();
                        a.removePhoto(album.get(index));
                        observableImageList.remove(index);
                        photoView.setItems(observableImageList);
                        photoView.getSelectionModel().select(0);
                        photoView.scrollTo(0);
                    }
                }
                write(albumList);
                photoView.getSelectionModel().select(0);
                photoView.scrollTo(0);
                if(album.size() != 0)
                    captionLabel.setText("Caption: " + album.get(0).getCaption());
                else {
                    captionLabel.setText("Caption: Album empty");
                    captionLabel.setVisible(false);
                    captionLabel.setDisable(true);
                }
                break;
            }
            case(8): {

                if(actionField.getText().equalsIgnoreCase("")) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error");
                    alert.setHeaderText("Please enter a valid album name");
                    alert.setResizable(false);
                    alert.showAndWait();
                    cancel();
                    break;
                }
                for (Album a : albumList) {
                    if (a.getAlbumName().equalsIgnoreCase(actionField.getText())) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error: An album with that name already exists");
                        alert.setResizable(false);
                        alert.showAndWait();
                        cancel();
                        actionField.setVisible(false);
                        actionField.setDisable(true);
                        tagField.setVisible(false);
                        tagField.setDisable(true);
                        confirmBar.setVisible(false);
                        albumLabel.setText("Search Results: ");
                        mainMenu.setDisable(false);
                        return;
                    }
                }

                Album newAlbum = new Album(actionField.getText());
                newAlbum.setAlbum(searchAlbum);
                albumList = read();
                albumList.add(newAlbum);
                write(albumList);

                actionField.setVisible(false);
                actionField.setDisable(true);
                tagField.setVisible(false);
                tagField.setDisable(true);
                confirmBar.setVisible(false);
                mainMenu.setDisable(false);

                exit();
            }
            default: return;
        }
        if (action < 6) {
            albumList = read();
            for (Album a : albumList) {
                if (a.getAlbumName().equalsIgnoreCase(currentAlbumName)) {
                    a.setAlbum(album);
                    break;
                }
            }
            write(albumList);
        }
        actionField.setVisible(false);
        actionField.setDisable(true);
        tagField.setVisible(false);
        tagField.setDisable(true);
        confirmBar.setVisible(false);
        albumLabel.setText(currentAlbumName + ": ");
        mainMenu.setDisable(false);
    }

    /** 
     * Function that cancels actions and returns to previous state
     */
    public void cancel() {
        actionField.setVisible(false);
        actionField.setDisable(true);
        tagField.setVisible(false);
        tagField.setDisable(true);
        confirmBar.setVisible(false);
        if (currentAlbumName == null) { albumLabel.setText("Search Results: "); }
        else { albumLabel.setText(currentAlbumName + ": "); }
        mainMenu.setDisable(false);
    }

    /**
     * Function that adds and displays new photo
     */
    public void newPhoto() {

        FileChooser chooseFile = new FileChooser();
        chooseFile.setTitle("Find Image File");
        chooseFile.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image file types","*.bmp","*.jpg","*.jpeg","*.png"));
        File selected = chooseFile.showOpenDialog(null);

        if(selected != null){
            TextInputDialog photoPathDialog = new TextInputDialog("");
            photoPathDialog.setTitle("Photo Information");
            photoPathDialog.setContentText("Enter name of photo inputted:");
            Optional<String> photoName = photoPathDialog.showAndWait();
            photoPathDialog.setContentText("Enter caption of photo inputted:");
            Optional<String> photoCaption = photoPathDialog.showAndWait();
            if(photoName.isPresent() && photoCaption.isPresent()){
                Photo photo = new Photo(selected, photoName.get(), photoCaption.get());
                for (Photo p : currentAlbum.getAlbum()) {
                    if (p.equals(photo)) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error: Photo already in album.");
                        alert.setResizable(false);
                        alert.showAndWait();
                        return;
                    }
                }
                album.add(photo);
                ImageView iv = new ImageView(photo.getThumbnail());
                iv.setFitWidth(100);
                iv.setFitHeight(100);
                observableImageList.add(iv);
                photoView.setItems(observableImageList);
                photoView.getSelectionModel().select(iv);
                photoView.scrollTo(iv);
                captionLabel.setText("Caption: " + photo.getCaption());
                captionLabel.setVisible(true);
                captionLabel.setDisable(false);
                albumList = read();
                for (Album a : albumList) {
                    if (a.getAlbumName().equalsIgnoreCase(currentAlbumName)) {
                        a.setAlbum(AlbumController.album);
                        break;
                    }
                }
                write(albumList);
            }
        }
    }

    /**
     * Function that logs out and returns to login stage
     */
    public void logout() {
        try {

            mainMenu.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Login.fxml"));
            Parent root = loader.load();

            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Login");
            stage.setScene(new Scene(root, 300, 250));
            stage.setResizable(false);

            stage.show();

        }
        catch (Exception e2){
            e2.printStackTrace();
        }
    }

    /**
     * Function that exists current album and returns to user stage
     */
    public void exit() {
        try {
            mainMenu.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/User.fxml"));
            Parent root = loader.load();
            UserController ctrl = loader.getController();

            Stage stage = new Stage();
            ctrl.start(stage);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle(UserController.getCurrentUser().getUsername());
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Function that sets up the renaming of photo selected
     */
    public void rename() {
        action = 1;
        actionField.setVisible(true);
        actionField.setDisable(false);
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /** 
     * Function that sets up the deletion of currently selected photo
     */
    public void delete() {
        action = 2;
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that sets up the changing of the caption
     */
    public void caption() {
        action = 3;
        actionField.setVisible(true);
        actionField.setDisable(false);
        actionField.setPromptText("Add Caption");
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that sets up adding a new tag to selected photo
     */
    public void addTag() {
        action = 4;
        actionField.clear();
        tagField.clear();
        actionField.setVisible(true);
        actionField.setDisable(false);
        actionField.setPromptText("Tag type (e.g. \"location\")");
        tagField.setVisible(true);
        tagField.setDisable(false);
        tagField.setPromptText("Tag value (e.g. \"New Brunswick\")");
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that sets up removing a tag from selecred photo
     */
    public void removeTag() {
        action = 5;
        actionField.clear();
        tagField.clear();
        actionField.setVisible(true);
        actionField.setDisable(false);
        actionField.setPromptText("Tag type (e.g. \"location\")");
        tagField.setVisible(true);
        tagField.setDisable(false);
        tagField.setPromptText("Tag value (e.g. \"New Brunswick\")");
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that makes a copy of photo to another album
     */
    public void copyTo() {
        action = 6;
        actionField.setVisible(true);
        actionField.setDisable(false);
        actionField.setPromptText("Enter Album");
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that moves currently selected photo to another album.
     * Deletes photo in current album
     */
    public void moveTo() {
        action = 7;
        actionField.setVisible(true);
        actionField.setDisable(false);
        actionField.setPromptText("Enter Album");
        mainMenu.setDisable(true);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
    }

    /**
     * Function that displays photos in a slideshow
     */
    public void slideshow() {
        if(photoView.getSelectionModel().getSelectedItem() != null) {
            ArrayList<Image> imageList = new ArrayList<>();
            ArrayList<String> tagList = new ArrayList<>();

            for (Photo p : currentAlbum.getAlbum()) { imageList.add(p.getThumbnail()); tagList.add(p.getTagString());}

            try {

                mainMenu.getScene().getWindow().hide();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/Slideshow.fxml"));
                Parent root = loader.load();
                SlideshowController ctrl = loader.getController();
                ctrl.setCurrentAlbum(imageList);
                ctrl.setPhotoTags(tagList);
                ctrl.setCurrentPhoto(photoView.getSelectionModel().getSelectedIndex());
                ctrl.setAlbumName(currentAlbumName);

                Stage stage = new Stage();
                ctrl.start(stage);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setOpacity(1);
                stage.setTitle("Slideshow");
                stage.setResizable(false);
                stage.setScene(new Scene(root));
                stage.show();

            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: no Photo selected.");
            alert.setResizable(false);
            alert.showAndWait();
        }
    }

    /**
     * Sets current user global to get user
     * @param user user to be set
     */
    static void setCurrentUser(User user){ currentUser = user; }

    /**
     * Sets current album global's name
     * @param albumName name of current album
     */
    void setCurrentAlbumName(String albumName) { this.currentAlbumName = albumName; }

    /**
     * maybe delete this?
     * @param albumList list of albums
     */
    private static void write(ArrayList<Album> albumList){
        try {
            ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("Users/" + currentUser.getUsername() + "/" +   FILENAME));
            stream.writeObject(albumList);
            stream.close();
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found");
            alert.setResizable(false);
            alert.showAndWait();
        }
    }

    /**
     * maybe delete this?
     * @return returns album list
     */
    private static ArrayList<Album> read(){
        File file = new File("Users/" + currentUser.getUsername() + "/" + FILENAME);
        ArrayList<Album> newAlbumList = null;
        try {
            if (file.length() == 0)
                file.createNewFile();
            else{
                ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
                newAlbumList = (ArrayList<Album>) stream.readObject();
                stream.close();
            }
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found2");
            alert.setResizable(false);
            alert.showAndWait();
        }
        return newAlbumList;
    }

    /**
     * Function that changes label to display caption of current selected
     * @param e item in list view selected click event
     */
    @SuppressWarnings("deprecation")
    @FXML public void mouseClick(MouseEvent e){
    	if(photoView.getSelectionModel().getSelectedItem() != null){
    		int index = photoView.getSelectionModel().getSelectedIndex();
    		captionLabel.setText("Caption: " + album.get(index).getCaption());
    	}
   }

    /**
     * Function that creates a new album from photos gotten from search
     */
    public void albumFromSearch() {
        mainMenu.setDisable(true);
        actionField.setVisible(true);
        actionField.setDisable(false);
        confirmBar.setVisible(true);
        confirmBar.setDisable(false);
        actionField.setPromptText("Album name");
        action = 8;
    }

    void setSearch(ArrayList<Photo> searchResults) {
        searchMenu.setVisible(true);
        photoSearchItem.setVisible(true);
        photoSearchItem.setDisable(true);
        searchCreateItem.setVisible(true);
        searchCreateItem.setDisable(false);
        searchAlbum = searchResults;
    }
}
