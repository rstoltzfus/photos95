package controller;

import javafx.scene.input.MouseEvent;
import model.Album;
import model.Photo;
import model.Tag;
import model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.LogManager;

/**
 * Class that shows user's view.
 * Shows albums for the current user and being able to enter an album.
 * Also allows addition/edit/deletion of any selected album
 * @author Benjamin Cahnbley
 * @author Richard Stoltzfus
 */
public class UserController {

    @FXML private MenuBar mainMenu;
    @FXML private MenuItem exitItem;
    @FXML private Menu editMenu;
    @FXML private MenuItem renameItem;
    @FXML private MenuItem deleteItem;
    @FXML private Menu searchMenu;
    @FXML private MenuItem photoSearchItem;
    @FXML private MenuItem searchCreateItem;

    @FXML private Label albumLabel;
    @FXML private Label startDate;
    @FXML private Label endDate;
    @FXML private Label searchAlbumName;
    @FXML private ListView<String> albumView;

    @FXML private TextField renameField;
    @FXML private TextField addField;

    @FXML private ButtonBar confirmAddBar;
    @FXML private ButtonBar confirmDeleteBar;
    @FXML private ButtonBar confirmRenameBar;
    @FXML private ButtonBar confirmSearchBar;
    @FXML private ButtonBar confirmSearchAddBar;

    @FXML private Button confirmAddButton;
    @FXML private Button confirmDeleteButton;
    @FXML private Button confirmRenameButton;
    @FXML private Button cancelRenameButton;
    @FXML private Button cancelButton;
    @FXML private Button enterAlbum;

    @FXML private ChoiceBox<String> andOrBox;
    @FXML private DatePicker earliestDate;
    @FXML private DatePicker latestDate;
    @FXML private TextField searchTagName;
    @FXML private TextField searchTagValue;
    @FXML private TextField searchName;

    @FXML private Label albumEarliestDate;
    @FXML private Label albumLatestDate;
    @FXML private Label albumCount;

    private static ArrayList<Album> albumList;
    private static User curr;
    private static final String FILENAME = "albumList.txt";
    private ObservableList<String> observableAlbumList;

    /**
     * Starts stage for user screen
     * @param mainStage the stage for user
     */
    void start(Stage mainStage) {

        albumList = read();
        if(albumList == null){
            albumList = new ArrayList<>();
            observableAlbumList = FXCollections.observableArrayList();
            albumView.setItems(observableAlbumList);
        }
        else{
            ArrayList<String> result = new ArrayList<>();
            for(Album a: albumList){
                result.add(a.getAlbumName());
            }
            observableAlbumList = FXCollections.observableArrayList(result);
            albumView.setItems(observableAlbumList);
        }
        albumView.getSelectionModel().select(0);
        if(albumView.getSelectionModel().getSelectedItem() != null){
            albumCount.setText("Photo Count: " + Integer.toString(albumList.get(0).getAlbumSize()));
            albumEarliestDate.setText("Earliest Date: " + albumList.get(0).getfirstDate());
            albumLatestDate.setText("Latest Date: " + albumList.get(0).getLastDate());
        }

        searchMenu.setVisible(true);
        photoSearchItem.setDisable(false);
        searchCreateItem.setDisable(true);
        renameField.setVisible(false);
        renameField.setDisable(true);
        addField.setDisable(true);
        addField.setVisible(false);

        confirmAddBar.setVisible(false);
        confirmAddBar.setDisable(true);
        confirmDeleteBar.setVisible(false);
        confirmDeleteBar.setDisable(true);
        confirmRenameBar.setVisible(false);
        confirmRenameBar.setDisable(true);

        earliestDate.setDisable(true);
        latestDate.setDisable(true);
        earliestDate.setVisible(false);
        latestDate.setVisible(false);
        confirmSearchBar.setVisible(false);
        confirmSearchBar.setDisable(true);
        searchTagName.setDisable(true);
        searchTagName.setVisible(false);
        searchTagValue.setVisible(false);
        searchTagValue.setDisable(true);

        searchName.setDisable(true);
        searchName.setVisible(false);
        startDate.setVisible(false);
        startDate.setDisable(true);
        endDate.setDisable(true);
        endDate.setVisible(false);
        searchAlbumName.setVisible(false);
        searchAlbumName.setDisable(true);
        confirmSearchAddBar.setDisable(true);
        confirmSearchAddBar.setVisible(false);
        andOrBox.setVisible(false);
        andOrBox.setDisable(true);

    }

    /**
     * Function that writes any edits to the list of albums for a user to file
     * @param albumList list of albums from current user
     */
    private void write(ArrayList<Album> albumList){
        try {
            ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("Users/" + curr.getUsername() + "/" +   FILENAME));
            stream.writeObject(albumList);
            stream.close();
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found");
            alert.setResizable(false);
            alert.showAndWait();
        }
    }

    /**
     * Reads array list of albums from file
     * @return array list of albums
     */
    private static ArrayList<Album> read(){
        File file = new File("Users/" + curr.getUsername() + "/" + FILENAME);
        ArrayList<Album> newAlbumList = null;

        try {
            if (file.length() == 0)
                file.createNewFile();
            else{
                ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
                newAlbumList = (ArrayList<Album>) stream.readObject();
                stream.close();
            }
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found2");
            alert.setResizable(false);
            alert.showAndWait();
        }
        return newAlbumList;
    }

    /**
     * Function that sets current user global
     * @param user user to be set
     */
    static void setCurrentUser(User user){ curr = user; }

    /**
     * get current user
     * @return current user
     */
    static User getCurrentUser(){ return curr; }

    /**
     * Function that gets the list of albums
     * @return list of albums
     */
    static ArrayList<Album> getAlbumList(){ return albumList; }

    /**
     * Function that confirms the addition of new album
     */
    public void confirmAdd() {

        LogManager.getLogManager().reset();
        for (Album a : albumList) {
            if (a.getAlbumName().equalsIgnoreCase(addField.getText())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error: Duplicate Album.");
                alert.setResizable(false);
                alert.showAndWait();
                return;
            }
        }

        if(addField.getText().trim().equals("")){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: Missing Album Name.");
            alert.setResizable(false);
            alert.showAndWait();
            return;
        }

        Album newAlbum = new Album(addField.getText());
        albumList.add(newAlbum);
        observableAlbumList.add(addField.getText().trim());
        albumView.setItems(observableAlbumList);
        for(int i = 0; i < observableAlbumList.size(); i++){
            if(observableAlbumList.get(i).equalsIgnoreCase(addField.getText().trim())){
                albumView.getSelectionModel().select(i);
                break;
            }
        }

        write(albumList);

        addField.clear();
        addField.setDisable(true);
        addField.setVisible(false);
        mainMenu.setDisable(false);

        confirmAddBar.setVisible(false);
        confirmAddBar.setDisable(true);
        mainMenu.setDisable(false);
        enterAlbum.setDisable(false);
    }
    
    /**
     * Function that confirms the deletion of currently selected album
     */
    public void confirmDelete() {

        int index = albumView.getSelectionModel().getSelectedIndex();
        LogManager.getLogManager().reset();

        for(int i =0; i < albumList.size(); i++){
            if(albumList.get(i).getAlbumName().equalsIgnoreCase(albumView.getSelectionModel().getSelectedItem()))
                albumList.remove(albumList.get(i));
        }
        observableAlbumList.remove(index,index+1);
        albumView.setItems(observableAlbumList);
        albumView.getSelectionModel().select(index);

        write(albumList);

        mainMenu.setDisable(false);
        confirmDeleteBar.setVisible(false);
        confirmDeleteBar.setDisable(true);
        enterAlbum.setDisable(false);
        enterAlbum.setVisible(true);

    }
    
    /**
     * Function that confirms the renaming of currently selected album
     */
    public void confirmRename() {
        if(renameField.getText().equalsIgnoreCase(""))
            return;
        for (Album a : albumList) {
            if (a.getAlbumName().equalsIgnoreCase(renameField.getText())) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Alert");
                alert.setHeaderText("Album with that name already exists.");
                alert.setResizable(false);
                alert.showAndWait();
                return;
            }
        }
        for (Album a : albumList) {
            if (a.getAlbumName().equalsIgnoreCase(albumView.getSelectionModel().getSelectedItem())) {
                a.setAlbumName(renameField.getText());
                observableAlbumList.set(albumList.indexOf(a), renameField.getText());
                albumView.setItems(observableAlbumList);
                break;
            }
        }

        write(albumList);

        renameField.clear();
        renameField.setDisable(true);
        renameField.setVisible(false);
        mainMenu.setDisable(false);
        confirmRenameBar.setVisible(false);
        confirmRenameBar.setDisable(true);
        mainMenu.setDisable(false);
        enterAlbum.setDisable(false);
    }

    /**
     * Function that cancels the current action
     */
    public void cancel() {

        renameField.clear();
        renameField.setVisible(false);
        renameField.setDisable(true);
        addField.clear();
        addField.setDisable(true);
        addField.setVisible(false);

        confirmAddBar.setVisible(false);
        confirmAddBar.setDisable(true);
        confirmDeleteBar.setVisible(false);
        confirmDeleteBar.setDisable(true);
        confirmRenameBar.setVisible(false);
        confirmRenameBar.setDisable(true);
        mainMenu.setDisable(false);
        enterAlbum.setDisable(false);

        earliestDate.setDisable(true);
        latestDate.setDisable(true);
        earliestDate.setVisible(false);
        latestDate.setVisible(false);
        confirmSearchBar.setVisible(false);
        confirmSearchBar.setDisable(true);
        searchTagName.setDisable(true);
        searchTagName.setVisible(false);
        searchTagValue.setVisible(false);
        searchTagValue.setDisable(true);

        confirmSearchAddBar.setVisible(false);
        confirmSearchAddBar.setDisable(true);
        searchName.clear();
        searchName.setDisable(true);
        searchName.setVisible(false);
    }

    /**
     * sets up the creation of a new album asking for required information
     */
    public void newAlbum() {
        mainMenu.setDisable(true);
        confirmAddBar.setDisable(false);
        confirmAddBar.setVisible(true);
        addField.setDisable(false);
        addField.setVisible(true);
        enterAlbum.setDisable(true);
    }

    /**
     * Logs out of current session.
     * Returns to log in stage
     */
    public void logout() {
        try {

            mainMenu.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Login.fxml"));
            Parent root = loader.load();


            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("login");
            stage.setScene(new Scene(root, 300, 250));
            stage.setResizable(false);

            stage.show();

        }
        catch (Exception e2){
            e2.printStackTrace();
        }
    }

    /**
     * Sets up renaming of currently selected album
     * Asks for new name of album
     */
    public void rename() {
        if(albumView.getSelectionModel().getSelectedItem() == null)
            return;
        if(observableAlbumList.size() == 0)
            return;

        mainMenu.setDisable(true);
        confirmRenameBar.setDisable(false);
        confirmRenameBar.setVisible(true);
        renameField.setVisible(true);
        renameField.setDisable(false);
        renameField.setPromptText("Album name");
        enterAlbum.setDisable(true);
    }

    /**
     * Sets up deletion of currently selected album
     */
    public void delete() {
        if(albumView.getSelectionModel().getSelectedItem() == null)
            return;
        if(observableAlbumList.size() == 0)
            return;

        mainMenu.setDisable(true);
        confirmDeleteBar.setDisable(false);
        confirmDeleteBar.setVisible(true);
        enterAlbum.setDisable(true);
    }

    /**
     * Sets up searching for pictures
     */
    public void searchPhotos() {
        mainMenu.setDisable(true);
        enterAlbum.setDisable(true);
        earliestDate.setDisable(false);
        latestDate.setDisable(false);
        earliestDate.setVisible(true);
        latestDate.setVisible(true);
        earliestDate.setPromptText("From: ");
        latestDate.setPromptText("To: ");
        confirmSearchBar.setVisible(true);
        confirmSearchBar.setDisable(false);
        searchTagName.setDisable(false);
        searchTagName.setVisible(true);
        searchTagName.setPromptText("Tag type (e.g. \"location\")");
        searchTagValue.setVisible(true);
        searchTagValue.setDisable(false);
        searchTagValue.setPromptText("Tag value (e.g. \"New Brunswick\")");
        andOrBox.setVisible(true);
        andOrBox.setDisable(false);
        andOrBox.setItems(FXCollections.observableArrayList("And", "Or"));
        andOrBox.getSelectionModel().select(0);
    }

    /**
     * Function that Opens selected album
     */
    public void openAlbum() {
        if(albumView.getSelectionModel().getSelectedItem() != null) {
            try {
                for (Album a: albumList) {
                    if(albumView.getSelectionModel().getSelectedItem().equalsIgnoreCase(a.getAlbumName()))
                        AlbumController.setAlbum(a.getAlbum());
                }

                mainMenu.getScene().getWindow().hide();

                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/view/Album.fxml"));
                Parent root = loader.load();
                AlbumController ctrl = loader.getController();
                ctrl.setCurrentAlbumName(albumView.getSelectionModel().getSelectedItem());

                Stage stage = new Stage();
                ctrl.start(stage);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.setOpacity(1);
                stage.setTitle("Photo Explorer");
                stage.setResizable(false);
                stage.setScene(new Scene(root));
                stage.show();

            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: no Album selected.");
            alert.setResizable(false);
            alert.showAndWait();
        }
    }

    /**
     * Function that searches and shows search results in temporary new album
     * @return new album formed from search
     */
    public Album confirmSearch() {
        Album searchAlbum = new Album("searchResults");
        Boolean validSearch = false;

        for (Album a: albumList) {
            for(Photo p: a.getAlbum()) {
                if(!searchTagName.getText().equals("") && !searchTagValue.getText().equals("")) {
                    validSearch = true;
                    for(Tag t: p.getTags()) {
                        if (andOrBox.getSelectionModel().isSelected(0)) {
                            //AND
                            if (t.getTagName().equalsIgnoreCase(searchTagName.getText())
                                    && t.getTagValue().equalsIgnoreCase(searchTagValue.getText())) {
                                boolean exists = false;
                                for (int i = 0; i < searchAlbum.getAlbumSize(); i++) {
                                    if (searchAlbum.getAlbum().get(i) == p) {
                                        exists = true;
                                        break;
                                    }
                                }
                                if (!exists) searchAlbum.addPhoto(p);
                            }
                        }
                        else if (andOrBox.getSelectionModel().isSelected(1)) {
                            //OR
                            if (t.getTagName().equalsIgnoreCase(searchTagName.getText())
                                    || t.getTagValue().equalsIgnoreCase(searchTagValue.getText())) {
                                boolean exists = false;
                                for (int i = 0; i < searchAlbum.getAlbumSize(); i++) {
                                    if (searchAlbum.getAlbum().get(i) == p) {
                                        exists = true;
                                        break;
                                    }
                                }
                                if (!exists) searchAlbum.addPhoto(p);
                            }
                        }
                    }
                }
                //only support search by tag OR dates
                else if(earliestDate.getValue() != null && latestDate.getValue() != null) {
                    validSearch = true;
                    if(p.getLocalDate().compareTo(earliestDate.getValue()) > 0
                            && p.getLocalDate().compareTo(latestDate.getValue()) < 0) {
                      boolean exists = false;
                      for (int i = 0; i < searchAlbum.getAlbumSize(); i++) {
                          if (searchAlbum.getAlbum().get(i).equals(p)) {
                              exists = true;
                              break;
                          }
                      }
                      if (!exists) searchAlbum.addPhoto(p);
                    }
                }
            }
        }
        if (!validSearch){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Alert");
            alert.setHeaderText("Invalid search parameters.");
            alert.setResizable(false);
            alert.showAndWait();
            return searchAlbum;
        }
        try {

            mainMenu.getScene().getWindow().hide();

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/view/Album.fxml"));
            Parent root = loader.load();
            AlbumController.setAlbum(searchAlbum.getAlbum());
            AlbumController ctrl = loader.getController();

            Stage stage = new Stage();
            ctrl.start(stage);
            ctrl.setSearch(searchAlbum.getAlbum());
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setOpacity(1);
            stage.setTitle("Photo Explorer");
            stage.setResizable(false);
            stage.setScene(new Scene(root));
            stage.show();

        }
        catch (Exception e2) {
            e2.printStackTrace();
        }

        mainMenu.setDisable(false);
        enterAlbum.setDisable(false);
        earliestDate.setDisable(true);
        latestDate.setDisable(true);
        earliestDate.setVisible(false);
        latestDate.setVisible(false);
        confirmSearchBar.setVisible(false);
        confirmSearchBar.setDisable(true);
        searchTagName.setDisable(true);
        searchTagName.setVisible(false);
        searchTagValue.setVisible(false);
        searchTagValue.setDisable(true);
        andOrBox.setVisible(false);
        andOrBox.setDisable(true);

        return searchAlbum;
    }

    /**
     * Function that displays information required for currently selected item
     * @param e list view selection click event
     */
    @FXML public void mouseClick(MouseEvent e){
        if(albumView.getSelectionModel().getSelectedItem() != null){
            for(Album a: albumList) {
                if(a.getAlbumName().equals(albumView.getSelectionModel().getSelectedItem())) {
                    albumCount.setText("Photo Count: " + Integer.toString(a.getAlbumSize()));
                    albumEarliestDate.setText("Earliest Date: " + a.getfirstDate());
                    albumLatestDate.setText("Latest Date: " + a.getLastDate());
                }
            }
        }
    }
}