package controller;

import model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.util.ArrayList;


/**
 * Class that displays login stage to log in into admin or a user
 * @author Benjamin Cahnbley
 * @author Richard Stoltzfus
 */
public class LoginController {

    public static ArrayList<User> userList = new ArrayList<User>();
    @FXML private TextField username;
    @FXML private PasswordField password;
    @FXML private Button login;
    public static final String FILENAME = "userList.txt";


    /**
     * Function that reads user objects from file
     * @return returns array list of all users 
     */
    public static ArrayList<User> read() {
        File file = new File(FILENAME);
        ArrayList<User> newUserList = null;

        try {
            if (file.length() == 0)
                file.createNewFile();
            else{
                ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
                newUserList = (ArrayList<User>) stream.readObject();
                stream.close();
            }
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found");
            alert.setResizable(false);
            alert.showAndWait();
        }
        return newUserList;
    }

    /**
     * Write any update to array list to file
     * @param userList array list of users
     */
    public static void write(ArrayList<User> userList){
        try {
            ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(FILENAME));
            stream.writeObject(userList);
            stream.close();
        }
        catch(Exception e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Error: File not found");
            alert.setResizable(false);
            alert.showAndWait();
        }
    }

    /**
     * Function that gets a list of users' names
     * @param list list of users
     * @return list of user's names
     */
    public static ArrayList<String> getUsersList(ArrayList<User> list){
        ArrayList<String> newUserList = new ArrayList<String>();
        for (int i = 0; i < list.size(); i++){
            newUserList.add(list.get(i).getUsername());
        }
        return newUserList;
    }

    /**
     * Function that attempts to log in with information entered
     * @param e button click event
     */
    public void login(ActionEvent e){
            if(username.getText().equals("") || password.getText().equals("")){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error");
                alert.setHeaderText("Error: No Username or password given");
                alert.setResizable(false);
                alert.showAndWait();
            }
            else if(username.getText().equalsIgnoreCase("admin") && password.getText().equalsIgnoreCase("admin")) {
                userList = read();
                if (userList == null)
                    userList = new ArrayList<User>();
                try {

                    ((Node) e.getSource()).getScene().getWindow().hide();

                    FXMLLoader loader = new FXMLLoader();
                    loader.setLocation(getClass().getResource("/view/Admin.fxml"));
                    Parent root = loader.load();
                    AdminController ctrl = loader.getController();


                    Stage stage = new Stage();
                    ctrl.start(stage);
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.setOpacity(1);
                    stage.setTitle("Admin");
                    stage.setResizable(false);
                    stage.setScene(new Scene(root));
                    stage.showAndWait();

                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
            else {
                    userList = read();
                    if (userList == null) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setTitle("Error");
                        alert.setHeaderText("Error: No users found.");
                        alert.setResizable(false);
                        alert.showAndWait();
                    }
                    else {
                        Boolean userFound = false;
                        for (User u : userList) {
                            if (username.getText().equalsIgnoreCase(u.getUsername())) {
                                userFound = true;
                                if (password.getText().equalsIgnoreCase(u.getPassword())) {
                                    try {
                                        UserController.setCurrentUser(u);
                                        AlbumController.setCurrentUser(u);

                                        ((Node) e.getSource()).getScene().getWindow().hide();

                                        FXMLLoader loader = new FXMLLoader();
                                        loader.setLocation(getClass().getResource("/view/User.fxml"));
                                        Parent root = loader.load();
                                        UserController ctrl = loader.getController();

                                        Stage stage = new Stage();
                                        ctrl.start(stage);
                                        stage.initModality(Modality.APPLICATION_MODAL);
                                        stage.setOpacity(1);
                                        stage.setTitle(username.getText());
                                        stage.setResizable(false);
                                        stage.setScene(new Scene(root));
                                        stage.showAndWait();

                                    } catch (Exception e2) {
                                        e2.printStackTrace();
                                    }
                                }
                                else {
                                    Alert alert = new Alert(Alert.AlertType.ERROR);
                                    alert.setTitle("Error");
                                    alert.setHeaderText("Error: Incorrect password.");
                                    alert.setResizable(false);
                                    alert.showAndWait();
                                }
                            }
                        }
                        if (!userFound) {
                            Alert alert = new Alert(Alert.AlertType.ERROR);
                            alert.setTitle("Error");
                            alert.setHeaderText("Error: User not found.");
                            alert.setResizable(false);
                            alert.showAndWait();
                        }
                    }
            }
    }
}
